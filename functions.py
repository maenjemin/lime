# coding=utf-8
import numpy as np

def calcEulerAngles(normalV):
    normal = [-x for x in normalV]
    # es gilt stets: phi = 0; dieser Wert wird nachfolgend ignoriert

    if(abs(normal[0]) < 1e-6 and abs(normal[1]) < 1e-6):
        psi=0
    else:
        a = np.pi/2 + np.arctan2(normal[1], normal[0])
        
        twoPi = 2*np.pi
        if(a<0):
            a += twoPi
        elif(a>twoPi):
            a -= twoPi
        
        psi = a * 180 / np.pi
    theta = np.arccos(normal[2])* 180/np.pi

    return((psi, theta))
