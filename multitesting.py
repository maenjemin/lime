# coding=utf-8
from stl import mesh    # used to import the binary stl-data
from tqdm import tqdm   # provides fancy status bar
import numpy as np      # mathematical structures and methods
import pcl              # small bindung to the PCL-Library

# Parallelize on CPU-cores:
from multiprocessing import Pool, cpu_count

from classes import *
from functions import *

import argparse

# Get paths to 3D-Data and other sys-argv
parser = argparse.ArgumentParser()
parser.add_argument('--turbine',dest='turbine', nargs='?',type=str, default='turbine.stl', help='Path zur Turbine')
parser.add_argument('--drill',dest='drill', nargs='?',type=str, default='drillbit.stl',help='Path zur Werkzeuggeometrie')
parser.add_argument('--millpath',dest='millpath', nargs='?',default='mill_path.xyz',type=str,help='Positionen des Fräskopfs')
parser.add_argument('--dest', dest='destination', nargs='?', default='distances.txt', type=str, help='Zielpfad zur Ausgabedatei')
parser.add_argument('--posN', dest='posN',nargs='?', default=False, type=int, help='Zum Testen: Anzahl der zu testenden Positionen')
parser.add_argument('--reduce', dest='reduce', default=[2,0.2], nargs=2, type=float,help='Nutze nur die ersten Prozent der Werkzeug-Geometrie entlang der zuerst definierten Achse (Standard: 2, 0.2 = Nutze nur 20% der Punkte, sortiert entlang der z-Achse.')
parser.add_argument('--additional', dest='additional', default=False, type=bool, help='If True, single-closest-point-pairs will be inclueded into output data.')

args = parser.parse_args()
turbine_path = args.turbine
millpath_path= args.millpath
drillbit_path = args.drill
file_path= args.destination
netw = args.networking  
additional = args.additional
reduction=[int(args.reduce[0]), args.reduce[1]]

print('Succesfully loaded all of the data. Now beginning our awesome calculations.')
d = mesh.Mesh.from_file(drillbit_path)
data = np.array(d.vectors).reshape(-1,3)

# only use first 20% of drill-data along z-Axis:
print("Sortiere Werkzeugpunkte entlang Achse {} und nutze dann {}% der {} Punkte.".format(reduction[0],100*reduction[1], len(data)))
ind = np.argsort(data[:, reduction[0]])
dpoints = data[ind]
dpoints = dpoints[-int(reduction[1]* len(dpoints)):] #as tool is oriented up, use last points

t = mesh.Mesh.from_file(turbine_path)
turb = Transformable3D(t.vectors)

positions = []
millIt = open(millpath_path, 'r')
for line in millIt:
    temp = line.split(';')
    positions.append([[float(comp) for comp in temp[0:3]], [float(comp) for comp in temp[3:]]])
millIt.close()

if( args.posN != 0):
    positions = positions[0:args.posN]
    splitNum = np.floor_divide(args.posN, cpu_count())
    print('Wir betrachten nur {} Positionen.'.format(args.posN))
else:
    splitNum = np.floor_divide(len(positions), cpu_count()*10)

posArray=np.array([position[0] for position in positions])
posInd = np.argsort(posArray[:,0])

# Construct the PointCloud and resulting Kd/AABB-Tree for target object 
turb_cl = pcl.PointCloud()
turb_cl.from_array(turb.points.astype(np.float32))
turb_kd = pcl.KdTreeFLANN(turb_cl)

# Function to be executed on CPU-cores; we will later iterate through all positions

def kdTreeDist(position):
    '''kdTreeDist(poisition) takes a list containing two sublists/-arrays, the first being the position, the second the normal.

    Works by first rotating and translating the tool by means of calculating the euler-angles and applying a rotation matrix as well as by simple vector-addition. Then a k-nearest-neighbor-search will be performed using PCL. This will return an array containing squared distances to all the points within the tool-geometry. The squareroot of the minimum is then added to a global dictionary.''' 
    global turb_kd,turb_cl, dpoints, additional
    tool = Transformable3D(dpoints)

    angles = calcEulerAngles(position[1])
    tool.CPUrotate( angles[0], angles[1])
    tool.CPUtranslate(position[0])
    ##GPUBruteforceDist(relPoints, drill.points)

    tool_cl= pcl.PointCloud()
    tool_cl.from_array(tool.points.astype(np.float32))

    indexes, sq_dists = turb_kd.nearest_k_search_for_cloud(tool_cl, 1)
    if(additional):
        distInd = np.argsort(sq_dists[:,0])
        MIN = np.sqrt(sq_dists[distInd[0]])
        fpoint = tool_cl[distInd[0]]
        tpoint = turb_cl[indexes[distInd[0],0]]
        return [position[0].tolist(), list(fpoint), list(tpoint), MIN[0]]
    
    MIN = np.sqrt(sq_dists[:,0].min())
    return [position[0].tolist(), MIN]

if (__name__ == "__main__"):
    p = Pool()
    distDict = []
    
    # Splitte hier für Debugging-Purposes das Position-Array in mehrere Teile, um eine Zeitabschätzung zu haben
    parts = np.array_split(positions,splitNum)
    for pos in tqdm(parts):
        results=p.map(kdTreeDist, pos)   
        distDict.extend(results)
    
    # multiprocessing may easily damage the order: resort
    distArray = np.array(distDict)
    # sort along positions in space:
    otherInd = np.argsort(distArray[:,0])
    distArray = distArray[otherInd]
    # undo that sorting with precalculated indices
    distArray = [distArray[ind] for ind in posInd]
    
    f = open(file_path, 'w')
    f.write("Nachfolgend: Pfadposition, Fräspunkt, Turbinenpunkt, Abstand\n")
    for key in distDict:
	if(additional):
            f.write('{};{};{};{} \n'.format(key[0],key[1],key[2],key[3]))
        else:
            f.write('{};{}\n'.format(key[0],key[1]))
    f.close()
    print("Abstände in Datei {} geschrieben.".format(file_path))
