Lime
====

Voraussetzungen & Installation
------------------------------
Die Software wurde in einer Linux-Umgebung entwickelt und getestet. Da aber Python als platformunabhängige Skript-Sprache gewählt wurde, sollte eine Nutzung unter Windows einfach möglich sein.

Voraussetzung ist  Python2.7 und die Pakete numpy, numpy-stl, tqdm und python-pcl.
Letzteres basiert auf der **PointCloudLibrary**, die in Ubuntu mittels Paketquellen und unter Windows mit Installer installiert werden kann. 

Wir empfehlen die Verwendung des Python-Bundles **Anaconda** (Versionsstrang 2), da mit dem intgrierten Paketmanager 'conda' eine sehr einfache Installation von python-pcl (und PCL) möglich ist. Mit pip und offiziellen Paketquellen hat sich das als schwieriger herausgestellt.

Unter Ubuntu können die restlichen Pakete wie gewohnt mit 'sudo pip install <paketname>' bereitgestellt werden.

Nutzung
-------

Aufruf des Programms erfolgt mit 'python multitesting.py'
Es können optional Kommandozeilenparameter übergeben werden. Eine Übersicht verschafft '--help'.
 
