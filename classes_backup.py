# coding=utf-8
import numpy as np
try:
    import reikna.cluda as cluda
    from reikna.linalg import MatrixMul as mm

except ImportError:
    reikna = False
    print("Die GPU-Algebra Bib. reikna steht nicht zur Verfügung")

class coolList(list):
    def first(self):
        return self[0]
    def second(self):
        return self[1]
    def third(self):
        return self[2]

class AABB():
    '''Corner und origin sollten bitte numpy-arrays sein ^^'''
    
    def __init__(self, origin, corner, container = False):
        self.origin = origin
        self.corner = corner
        self.container = container
        self.points = list()
##
##    def __addAABB__(self, BoundingBox):
##        self.subs.append(BoundingBox)
##        self.container = 1

    def __fill__(self ,pointList):
        self.points.extend(pointList)
        self.container = 1

    def __intersects__(self, other):
        # Do Boxes intersect along x-Axis?
        if( self.origin[0] <= other.origin[0] <= self.corner[0] or self.origin[0] <= other.corner[0] <= self.corner[0]):
            # And along y and z?
            if( self.origin[1] <= other.origin[1] <= self.corner[1] or self.origin[1] <= other.corner[1] <= self.corner[1]):
                if( self.origin[2] <= other.origin[2] <= self.corner[2] or self.origin[2] <= other.corner[2] <= self.corner[2]):
                    return 1

        else:
            return 0

        
    def translate(self, vector):
        self.origin = self.origin + vector
        self.corner = self.corner + vector

class AABBTree():
    def __init__(self,depth = 5, minLeafPoints = 40):
        self.depth = depth
        self.minLP = minLeafPoints

    def __generate__(self, pointList):
        print('Generiere AABB-Baumstruktur aus den {} Punkten.'.format(len(pointList)))
        vals = minMaxCompsOfVecs (pointList)
        firstbox = AABB([vals['mX'],vals['mY'], vals['mZ']], [vals['MX'], vals['MY'], vals['MZ']])
        firstbox.__fill__(pointList)
        self.nodes = [[firstbox]]
        cont=True

        for i in tqdm(range(1, self.depth+1)):
            if (cont == 0):
                self.nodes.remove([])
                break

            node = self.nodes[i-1]
            self.nodes.append([])
            sumLevel = 0

            for box in node:                
                oldList = box.points
                half = int(len(oldList)/2)

                if(half < self.minLP):
                    cont = 0
                    self.depth=i-1
                    break
                
                #print('In der vorherigen Box (Ebene {}) sind {} Punkte.'.format(i-1,len(oldList)))
                sumLevel += len(oldList)
                vals = minMaxCompsOfVecs(oldList)

                # In the following we gonna 'cut' the given Box along its longest dimension
                lenX = vals['MX'] - vals['mX']
                lenY= vals['MY'] - vals['mY']
                lenZ = vals['MZ'] - vals['mZ']

                maxLen = max( lenX, lenY, lenZ)
                
                if(maxLen == lenX):
                    oldList.sort(key=coolList.first)
                elif(maxLen == lenY):
                    oldList.sort(key=coolList.second)
                else:
                    oldList.sort(key=coolList.third)

                list1 = oldList[0:half]
                list2 = oldList[half:]

                new1 = minMaxCompsOfVecs(list1)
                new2 = minMaxCompsOfVecs(list2)
                sub1 = AABB([new1['mX'], new1['mY'], new1['mZ']], [new1['MX'], new1['MY'],new1['MZ']])
                sub2 = AABB([new2['mX'], new2['mY'], new2['mZ']], [new2['MX'], new2['MY'],new2['MZ']])
                sub1.__fill__(list1)
                sub2.__fill__(list2)

                self.nodes[i].append(sub1)
                self.nodes[i].append(sub2)
                
            #print('Damit hat die gesamte Ebene {} Punkte'.format(sumLevel))

class Transformable3D():
    def __init__(self, pointList):

        # points sollte eine n×3-Matrix sein mit n = #Punkte
        if (type(pointList) == np.ndarray):
            self.points = pointList.reshape(-1,3)
        else:
            self.points = np.array(pointList)

        self.POINTS = self.points

    def CPUrotate(self,psi, theta):
        cPsi = np.cos(psi)
        sPsi = np.sin(psi)
        cTheta = np.cos(theta)
        sTheta = np.sin(theta)

        M = [
        [cPsi, sPsi, 0],
        [-sPsi, cPsi*cTheta, sTheta],
        [sPsi*sTheta, -cPsi*sTheta, cTheta]]

        bodyBefore = self.points.transpose()
        bodyAfter = np.dot(M,bodyBefore).transpose()

        self.points = bodyAfter #.tolist()  # Ab jetzt wird  mit Arrays gerechnet
    def GPUrotate(self, psi, theta):
        cPsi = np.cos(psi)
        sPsi = np.sin(psi)
        cTheta = np.cos(theta)
        sTheta = np.sin(theta)

        M = np.array( [
        [cPsi, sPsi, 0],
        [-sPsi, cPsi*cTheta, sTheta],
        [sPsi*sTheta, -cPsi*sTheta, cTheta]], dtype=np.float32)

        api = cluda.cuda_api()
        thr = api.Thread.create()
        body_dev = thr.to_device(self.points.transpose())
	M_dev = thr.to_device(M)
	res_dev = thr.array( (3, self.points.shape[0]), dtype=np.float32)
        dot = mm(M_dev, body_dev, out_arr = res_dev)
        dotc = dot.compile(thr)
        dotc(res_dev, M_dev, body_dev)
        self.points = res_dev.get().transpose()



    def CPUtranslate(self, point):
        if (len(point)!=3):
            print('Fehler: Der gegebene Translationsvektor hat die falsche Anzahl Komponenten.')
            return 0
        self.points[:,0] = [x + point[0] for x in self.points[:,0]]
        self.points[:,1] = [y + point[1] for y in self.points[:,1]]
        self.points[:,2] = [z + point[2] for z in self.points[:,2]]

    def reset(self):
        self.points = self.POINTS
