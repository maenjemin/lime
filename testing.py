# coding=utf-8
from stl import mesh
from tqdm import tqdm
import numpy as np
from time import time
import pcl
try:
    import pycuda.driver as cuda
    import pycuda.autoinit
    from pycuda.compiler import SourceModule
    gpu = True

except ImportError:
    gpu = False
    print("Die Bibliotheken pycuda oder reikna konnten nicht geladen werden.\nDamit stehen die GPU-Funktionen nicht zur Verfügung")

from classes import *
from functions import *

turbine_path = 'turbine.stl'
millpath_path = 'mill_path.xyz'
drillbit_path = 'drillbit.stl'

### Erste Testläufe (?)

d = mesh.Mesh.from_file(drillbit_path)
drill = Transformable3D(d.vectors)

t = mesh.Mesh.from_file(turbine_path)
turb = Transformable3D(t.vectors)

positions = []
millIt = open(millpath_path, 'r')
for line in millIt:
    temp = line.split(';')
    positions.append([[float(comp) for comp in temp[0:3]], [float(comp) for comp in temp[3:]]])
millIt.close()

turb_cl = pcl.PointCloud()
turb_cl.from_array(turb.points.astype(np.float32))
turb_kd = pcl.KdTreeFLANN(turb_cl)
for i in tqdm(range(0, len(positions))):
    drill.reset()

    position = positions[i]
    angles = calcEulerAngles(position[1])
    drill.GPUrotate( angles[0], angles[1])
    drill.CPUtranslate(position[0])
    ##GPUBruteforceDist(relPoints, drill.points)

    drill_cl= pcl.PointCloud()
    drill_cl.from_array(drill.points.astype(np.float32))
    #drill_kd = pcl.KdTreeFLANN(drill_cl)

    indexes, sq_dists = turb_kd.nearest_k_search_for_cloud(drill_cl, 1)

#print("Der Vorgang hat {} Sekunden gedauert. Die kleinste Distanz ist {}.".format(t1-t0, np.sqrt(sq_dists.min())))
     


