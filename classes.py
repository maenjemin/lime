# coding=utf-8
import numpy as np

class Transformable3D():
    def __init__(self, pointList):

        # points sollte eine n×3-Matrix sein mit n = #Punkte
        if (type(pointList) == np.ndarray):
            self.points = pointList.reshape(-1,3)
        else:
            self.points = np.array(pointList).reshape(-1,3)

        self.POINTS = self.points

    def CPUrotate(self,psi, theta):
        cPsi = np.cos(psi)
        sPsi = np.sin(psi)
        cTheta = np.cos(theta)
        sTheta = np.sin(theta)

        M = [
        [cPsi, sPsi, 0],
        [-sPsi*cTheta, cPsi*cTheta, sTheta],
        [sPsi*sTheta, -cPsi*sTheta, cTheta]]

        bodyBefore = self.points.transpose()
        bodyAfter = np.dot(M,bodyBefore).transpose()

        self.points = bodyAfter #.tolist()  # Ab jetzt wird  mit Arrays gerechnet

    def CPUtranslate(self, point):
        if (len(point)!=3):
            print('Fehler: Der gegebene Translationsvektor hat die falsche Anzahl Komponenten.')
            return 0
        self.points[:,0] = [x + point[0] for x in self.points[:,0]]
        self.points[:,1] = [y + point[1] for y in self.points[:,1]]
        self.points[:,2] = [z + point[2] for z in self.points[:,2]]

    def reset(self):
        self.points = self.POINTS
