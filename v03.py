from stl import mesh
from tqdm import tqdm
import numpy as np
from time import time

try:
    import pycuda.driver as cuda
    import pycuda.autoinit
    from pycuda.compiler import SourceModule

    # Bibliotheken für GPU-basierte MatrixMultiplikation (Rotation der Fräse)
    import reikna.cluda as cluda
    from reikna.linalg import MatrixMul

    gpu = True

except ImportError:
    gpu = False
    print("Die Bibliotheken pycuda oder reikna konnten nicht geladen werden.\nDamit stehen die GPU-Funktionen nicht zur Verfügung")

from classes import *
from functions import *

turbine_path = 'turbine.stl'
millpath_path = 'mill_path.xyz'
drillbit_path = 'drillbit.stl'

guesses = 3
        
####################################################################################
# Teste die Baumstruktur mit dem gegebenen Pfad
##path = coolList()
##millIt = open(millpath_path, 'r')
##for line in tqdm(millIt):
##    temp = line.split(';')
##    path.append([float(comp) for comp in temp[0:3]])
##
##millIt.close()

##tree = AABBTree(depth=20)
##tree.__generate__(path)

#####################################################################################
    
### Erste Testläufe (?)

d = mesh.Mesh.from_file(drillbit_path)
drill = Transformable3D(d.vectors)

t = mesh.Mesh.from_file(turbine_path)
turb = Transformable3D(t.vectors)

positions = []
millIt = open(millpath_path, 'r')
for line in millIt:
    temp = line.split(';')
    positions.append([[float(comp) for comp in temp[0:3]], [float(comp) for comp in temp[3:]]])
millIt.close()

### Lege die zufälligen Testpositionen fest:

rands = np.random.randint(0, len(positions), guesses).tolist()
testPos = [ positions[r] for r in rands ]
#estimate = estimateDistance(turb.points, drill, testPos) # <-- Dauert sehr lange, baue noch 'translateTillHit' oder so

### Ermittle jetzt bespielhaft Distanz für eine Fräsposition

drill.reset()
##print("Baue Turbinen-AABB-Tree.")
##turbTree = AABBTree()
### intermezzo fürn alten lappi:
##sel = [ k%2 for k in range(0, len(turb.points)) ]
##punkte = turb.points.compress(sel, axis=0)
##sel = [k%2 for k in range(0, len(punkte))]
##punkte = punkte.compress(sel, axis=0)
##sel = [k%2 for k in range(0, len (punkte))]
##punkte = punkte.compress(sel, axis=0)
##turbTree.__generate__(punkte.tolist())

print("Jetzt ums Werkzeug kümmern.")
t0= time()

position = positions[np.random.randint(0,70000)]
angles = calcEulerAngles(position[1])
drill.CPUrotate( angles[0], angles[1])
drill.CPUtranslate(position[0])
##
##mM = minMaxCompsOfVecs(drill.points.tolist())
##origin = lct(mM)['min']
##corner = lct(mM)['max']
##
##drillBox = AABB(origin, corner)
##
##colResults = translateTillHit(drillBox, turbTree, np.array(position[1]))
##print("Die AABBs kollidieren bei Translation um das {}-fache des Normalenvektors".format(colResults[1]))
##relBoxes= colResults[0] #boxVsTree(drillBox, turbTree,depth=5)
##
##relPoints=[]
##for elem in relBoxes:
##    relPoints.extend(elem.points)
##print("Für diese Position werden {} Punkte der Turbinengeometrie betrachtet.".format(len(relPoints)))
##GPUBruteforceDist(relPoints, drill.points)
numpo= 10000
print("Erstelle zufällig {} Punkte im R^3 und ermittle den Min Abstand auf der GPU".format(numpo))
zufall = np.random.randn(10000,3)
GPUBruteforceDist(zufall, drill.points)
t1 = time()

print("Der Vorgang hat {} Sekunden gedauert.".format(t1-t0))
     


