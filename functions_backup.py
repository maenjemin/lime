# coding=utf-8
import numpy as np
def minMaxCompsOfVecs(point_list):
    
    point_list.sort(key=coolList.first)
    minX = point_list[0][0]
    maxX = point_list[-1][0]

    # Now do the same for y and z
    point_list.sort(key=coolList.second)
    minY = point_list[0][1]
    maxY = point_list[-1][1]

    point_list.sort(key=coolList.third)
    minZ = point_list[0][2]
    maxZ = point_list[-1][2]

    return({'mX':minX,'MX':maxX, 'mY':minY, 'MY':maxY, 'mZ':minZ, 'MZ':maxZ})

def lct(mMdict):
    return({'min':np.array([mMdict['mX'], mMdict['mY'], mMdict['mY']]), 'max':np.array([mMdict['MX'], mMdict['MY'], mMdict['MZ'] ])})

def boxVsTree(box, tree, depth = 5):
    """ Function takes an AABB-Box as well as an AABB-Tree and returns a list of intersecting boxes at the level defined by depth."""

    other = tree.nodes[0][0]
    if not(box.__intersects__(other)):
        return(0)
    colInd = [0]
    for i in range(1, depth):
        newInd =[]
        for k in colInd:
            other1 = tree.nodes[i][2*k]
            other2 = tree.nodes[i][2*k+1]

            if(box.__intersects__(other1)):
                newInd.append(2*k)
            if(box.__intersects__(other2)):
                newInd.append(2*k+1)
        colInd = newInd

    relevantBoxes = [ tree.nodes[depth-1][k] for k in colInd ]
    return(relevantBoxes)

def calcEulerAngles(normalV):
    normal = [-x for x in normalV]
    # es gilt stets: phi = 0; dieser Wert wird nachfolgend ignoriert

    if(abs(normal[0]) < 1e-6 and abs(normal[1]) < 1e-6):
        psi=0
    else:
        a = np.pi/2 + np.arctan2(normal[1], normal[0])
        
        twoPi = 2*np.pi
        if(a<0):
            a += twoPi
        elif(a>twoPi):
            a -= twoPi
        
        psi = a * 180 / np.pi
    theta = np.arccos(normal[2])* 180/np.pi

    return((psi, theta))

def CPUBruteforceDist(points1, points2):
    """Berechnet die minimale Distanz der gegebene Punktlisten (/-Arrays)."""

    if( not type(points1) == np.array):
        points1 = np.array(points1).reshape(-1,3)
    if(not type(points2) == np.array):
        points2 = np.array(points2).reshape(-1,3)

    dists = []
    print("Berechne die minimale Distanz beider Objekte. Vergleich dazu {} mit {} Punkten im R^3:".format(len(points1), len (points2)))
    for a in tqdm(points1):
        for b in points2:
          x = a[0]-b[0]; x *= x;
          y = a[1]-b[1]; y *= y;
          z = a[2]-b[2]; z *= z;

          dists.append(x+y+z)

    dists.sort()
    minDist = np.sqrt(dists[0])
    print("Die minimale Distanz ist {}.".format(minDist))
    return(minDist)

def estimateDistance(body, drill, positionList):
    """Calculates the exact (pairwaise) distance between the main body and the drill for given positions.

    positionList should look somewhat like [[px,py,pz],[nx,ny,nz]]
    drill should be of class Transformable3D
    body a n×3 numpy-array"""
    print("Es wird für {} Position der Abstand mittels Punkt zu Punkt ermittelt, um später die AABBs zu translatieren".format(len(positionList)))

    dists = []
    for elem in positionList:
        angles = calcEulerAngles(elem[1])
        drill.CPUrotate( angles[0], angles[1])
        drill.CPUtranslate( elem[0])

        if gpu:
            GPUBruteforceDist(body, drill.points)
        else:
            CPUBruteforceDist(body,drill.points)

        drill.reset()

    med = np.mean(dists)
    print("Die ermittleten Distanzen sind {} und der Mittelwert ist {}".format(dists, med))
    return (med)

def setCudaVars():
    pass

def GPUBruteforceDist(bigArray, smallArray):
    '''Calculates the distance between each possible pair of points in both numpy-arrays and returns the minimum.

    Runs on the GPU and requires a CUDA-enabled workspace.
    Due to memory and timeout issues we have to iterate through every point of smallArray and launch a grid containing enough threads to let each thread calculate the distance between on point of bigArray and the one point at iteration level.'''

    #some lines will be omitted wen setCudaVars is ready
    dev = pycuda.autoinit.device
    tpB = dev.get_attribute(cuda.device_attribute.MAX_THREADS_PER_BLOCK)
                            
    sizeB = len(bigArray)
    sizeS = len(smallArray)

    # determine grid size
    even32 = [32, 64, 128, 256, 512]
    for i in range(0, len(even32)-1):
        needed = np.sqrt(sizeB/tpB)
        if(needed <= 32):
            dim = 32
        elif(even32[i] < needed <= even32[i+1]):
            dim = even32[i+1]

    # linearize first array
    B = bigArray.reshape(-1)
    distances = []

    mod_template = '''
        __global__ void distance(float *B, float *point, float *dists, int numB){
        __shared__ float cache[%(THREADS)s];
        int bid = blockIdx.x + blockIdx.y * gridDim.x;
        int tid = threadIdx.x ;
        int uid = tid + bid * blockDim.x;

        while( uid < numB){
            float x1 = B[3*bid], x2=point[0], y1=B[3*bid+1], y2=point[1], z1=B[3*bid+2], z2=point[2];
            float s1 = x1 - x2, s2=y1 - y2, s3= z1 - z2;

            float dist = s1*s1 + s2*s2 + s3*s3;
            cache[tid] = dist;

            uid += blockDim.x * gridDim.x;
        }
        __syncthreads();

        int j = blockDim.x/2;
        while (j!=0){
            if(tid<j){
                cache[tid] = (cache[tid] < cache[tid+j]) ? cache[tid]:cache[tid+j];
            }
            __syncthreads();
            j /= 2;
        }

        if(tid == 0){
            dists[bid] = cache[tid];
        }
        }
        '''
    kernel = mod_template % {
        'THREADS':tpB}

    for point in tqdm( smallArray, leave=1):
        POINT = np.array(point).reshape(-1)
        dists = np.zeros(dim*dim, dtype=np.float32)

        B_dev = cuda.mem_alloc(B.nbytes)
        point_dev = cuda.mem_alloc(POINT.nbytes)
        D_dev = cuda.mem_alloc(dists.nbytes)

        cuda.memcpy_htod(B_dev, B)
        cuda.memcpy_htod(point_dev, POINT)
       
        mod = SourceModule(kernel)

        func = mod.get_function('distance')
        func(B_dev, point_dev, D_dev, np.int32(sizeB), grid=(dim,dim), block=(tpB,1,1))

        cuda.memcpy_dtoh(dists, D_dev)
        masked_dists = np.ma.masked_equal(dists, 0.0, copy=False)
        mdist=min(masked_dists)
        distances.append(mdist)

    mindist= np.sqrt(min(distances))
    print(mindist)
    return (mindist)

def translateTillHit(box, tree, vector, estimate = -1):
    vec=(-1)*vector
    box.translate(estimate*vec)
    hit = boxVsTree(box, tree)
    i = 1
    while(hit == 0):
        box.translate(vector)
        hit = boxVsTree(box, tree)
        print("duiaerr")
        i += 1
    return ((hit, i))
